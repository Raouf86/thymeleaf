package pl.cylkowski.thymeleaf.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.cylkowski.thymeleaf.models.User;

@Controller
public class IndexController {

    @GetMapping("/")
    public String show(Model model){
        model.addAttribute("name", "Marcin");
        return "index";
    }

    @GetMapping("/nowy")
    public String page(Model model){
        User user = new User("marcin", "password123");
        model.addAttribute("user", user);
        return "page";
    }

}
